# First Repo on bitbucket

This is a simple test meant to create a local repository via cmdline and push it to bitbucket repo.

Steps:

0. chose a local directory
1. git init .
2. insert REAMDE.md and .gitignore
3. git add .
4. (optional but recomanded) git status
5. git commit -m "Commit message"
6. git remote add origin https://user@bitbucket.org/user/repository.git
7. git push -u origin -all
